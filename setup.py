# -*- coding: utf-8 -*-
"""
package/install module package aehostd
"""

import sys
import os
from setuptools import setup, find_packages

PYPI_NAME = 'aehostd'

BASEDIR = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, os.path.join(BASEDIR, PYPI_NAME))
import __about__

setup(
    name=PYPI_NAME,
    license=__about__.__license__,
    version=__about__.__version__,
    description='AE-DIR host demon',
    long_description='AE-DIR host client demon for NSS and PAM',
    author=__about__.__author__,
    author_email=__about__.__mail__,
    maintainer=__about__.__author__,
    maintainer_email=__about__.__mail__,
    url='https://ae-dir.com/aehostd.html',
    download_url='https://pypi.org/project/%s/#files' % (PYPI_NAME),
    keywords=['LDAP', 'LDAPv3', 'OpenLDAP', 'AE-DIR', 'Æ-DIR'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'Intended Audience :: Telecommunications Industry',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 2 :: Only',
        'Topic :: System :: Systems Administration',
        'Topic :: System :: Systems Administration :: Authentication/Directory',
        'Topic :: System :: Systems Administration :: Authentication/Directory :: LDAP',
    ],
    packages=find_packages(exclude=['tests']),
    package_dir={'': '.'},
    test_suite='tests',
    python_requires='==2.7.*',
    include_package_data=True,
    data_files=[],
    install_requires=[
        'setuptools',
        'lockfile',
        'python-daemon',
        'ldap0>=0.0.63',
        'aedir>=0.1.7',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': [
            # the main NSS/PAM service
            '{0}={0}.srv:main'.format(PYPI_NAME),
            # the privileged helper service
            '{0}-ph={0}.priv:main'.format(PYPI_NAME),
        ],
    }
)

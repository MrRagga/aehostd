# -*- coding: utf-8 -*-
"""
aehostd.group - group entry lookup routines (group map)
"""

from __future__ import absolute_import

import logging

from .cfg import CFG
from . import req
from .passwd import PASSWD_MAP

GROUP_MAP = {}
GROUP_MAP_VKEYS = GROUP_MAP.viewkeys()
GROUP_NAME_MAP = {}
GROUP_MEMBER_MAP = {CFG.aehost_vaccount[0]: []}

NSLCD_ACTION_GROUP_BYNAME = 0x00040001
NSLCD_ACTION_GROUP_BYGID = 0x00040002
NSLCD_ACTION_GROUP_BYMEMBER = 0x00040006
NSLCD_ACTION_GROUP_ALL = 0x00040008


class GroupReq(req.Request):

    def write(self, result):
        name, passwd, gid, members = result
        self.tios.write_string(name)
        self.tios.write_string(passwd)
        self.tios.write_int32(gid)
        self.tios.write_stringlist(members)


class GroupByNameReq(GroupReq):

    action = NSLCD_ACTION_GROUP_BYNAME

    def read_parameters(self):
        name = self.tios.read_string()
        return dict(cn=name)

    def get_results(self, parameters):
        if parameters['cn'] in CFG.nss_ignore_groups:
            self._log(logging.DEBUG, 'ignore requested group %r', parameters['cn'])
            return
        try:
            res = GROUP_MAP[GROUP_NAME_MAP[parameters['cn']]]
        except KeyError:
            self._log(logging.DEBUG, 'not found %r', parameters)
            return
        yield res


class GroupByGidReq(GroupReq):

    action = NSLCD_ACTION_GROUP_BYGID

    def read_parameters(self):
        return dict(gidNumber=self.tios.read_int32())

    def get_results(self, parameters):
        gid = parameters['gidNumber']
        if gid < CFG.nss_min_gid or \
           gid > CFG.nss_max_gid or \
           gid in CFG.nss_ignore_gids:
            self._log(logging.DEBUG, 'ignore requested GID %d', gid)
            return
        try:
            res = GROUP_MAP[gid]
        except KeyError:
            self._log(logging.DEBUG, 'not found %r', parameters)
            return
        yield res


class GroupByMemberReq(GroupReq):

    action = NSLCD_ACTION_GROUP_BYMEMBER

    def read_parameters(self):
        memberuid = self.tios.read_string()
        return dict(memberUid=memberuid)

    def get_results(self, parameters):
        member_uid = parameters['memberUid']
        if member_uid in CFG.nss_ignore_users:
            self._log(logging.DEBUG, 'ignore requested memberUid %r', member_uid)
            return
        for gid in GROUP_MEMBER_MAP.get(member_uid, []):
            name, passwd, gid, _ = GROUP_MAP[gid]
            yield (name, passwd, gid, [member_uid])


class GroupAllReq(GroupReq):

    action = NSLCD_ACTION_GROUP_ALL

    def get_results(self, parameters):
        for _, group_entry in GROUP_MAP.items():
            yield group_entry

# -*- coding: utf-8 -*-
"""
aehostd.passwd - lookup functions for user account information (passwd map)
"""

from __future__ import absolute_import

import logging

from .cfg import CFG
from . import req


PASSWD_MAP = {}
PASSWD_MAP_VKEYS = PASSWD_MAP.viewkeys()
PASSWD_NAME_MAP = {}

NSLCD_ACTION_PASSWD_BYNAME = 0x00080001
NSLCD_ACTION_PASSWD_BYUID = 0x00080002
NSLCD_ACTION_PASSWD_ALL = 0x00080008


class PasswdReq(req.Request):

    def write(self, result):
        name, passwd, uid, gid, gecos, home, shell = result
        self.tios.write_string(name)
        self.tios.write_string(passwd)
        self.tios.write_int32(uid)
        self.tios.write_int32(gid)
        self.tios.write_string(gecos)
        self.tios.write_string(home)
        self.tios.write_string(shell)

class PasswdByNameReq(PasswdReq):

    action = NSLCD_ACTION_PASSWD_BYNAME

    def read_parameters(self):
        name = self.tios.read_string()
        return dict(uid=name)

    def get_results(self, parameters):
        username = parameters['uid']
        if username in CFG.nss_ignore_users:
            self._log(logging.DEBUG, 'ignore requested user %r', username)
            return
        try:
            res = PASSWD_MAP[PASSWD_NAME_MAP[username]]
        except KeyError:
            self._log(logging.DEBUG, 'not found %r', parameters)
            return
        yield res


class PasswdByUidReq(PasswdReq):

    action = NSLCD_ACTION_PASSWD_BYUID

    def read_parameters(self):
        return dict(uidNumber=self.tios.read_int32())

    def get_results(self, parameters):
        userid = parameters['uidNumber']
        if userid < CFG.nss_min_uid or \
           userid > CFG.nss_max_uid or \
           userid in CFG.nss_ignore_uids:
            self._log(logging.DEBUG, 'ignore requested UID %d', userid)
            return
        try:
            res = PASSWD_MAP[userid]
        except KeyError as err:
            self._log(logging.DEBUG, '%r not found: %s', parameters, err)
            return
        yield res


class PasswdAllReq(PasswdReq):

    action = NSLCD_ACTION_PASSWD_ALL

    def get_results(self, parameters):
        for _, passwd_entry in PASSWD_MAP.items():
            yield passwd_entry

"""
aehostd.service - Unix domain socket server
"""

from __future__ import absolute_import

import sys
import os
import socket
import SocketServer
import time
import logging
import logging.handlers
import struct
import argparse
import signal

from lockfile.pidlockfile import PIDLockFile
import daemon

from .__about__ import __version__
from . import cfg
from .cfg import CFG
from . import req
from .tiostream import TIOStream


SO_PEERCRED_DICT = {
    'linux2': (17, '3i'), # for Linux systems
}

# default umask used
UMASK_DEFAULT = 0022

# log format to use when logging to syslog
SYS_LOG_FORMAT = '%(levelname)s %(message)s'

# log format to use when logging to console
CONSOLE_LOG_FORMAT = '%(asctime)s %(levelname)s %(message)s'


def cli_args(script_name, service_desc):
    """
    CLI arguments
    """
    parser = argparse.ArgumentParser(
        prog=script_name,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=service_desc,
    )
    parser.add_argument(
        '-f', '--config',
        dest='cfg_filename',
        default='/etc/aehostd.conf',
        help='configuration file name',
        required=False,
    )
    parser.add_argument(
        '-p', '--pid',
        dest='pidfile',
        default=os.path.join('/', 'var', 'run', 'aehostd', script_name)+'.pid',
        help='PID file name',
        required=False,
    )
    parser.add_argument(
        '-l', '--log-level',
        dest='log_level',
        default=None,
        help='log level',
        type=int,
        required=False,
    )
    parser.add_argument(
        '-n', '--no-fork',
        dest='no_fork',
        default=False,
        help='Do not fork or daemonise, run in the foreground.',
        action='store_true',
        required=False,
    )
    parser.add_argument(
        '-c', '--check',
        dest='check_only',
        default=False,
        help='Check whether demon is running.',
        action='store_true',
        required=False,
    )
    return parser.parse_args()
    # end of cli_args()


def init_logger(log_name, log_level, no_fork):
    """
    Returns a combined SysLogHandler/StreamHandler logging instance
    with formatters
    """
    if CFG.logsocket is None and no_fork:
        # send messages to stderr (console)
        log_format = CONSOLE_LOG_FORMAT
        log_handler = logging.StreamHandler()
    else:
        # send messages to syslog
        log_format = '{name} {fmt}'.format(
            name=log_name,
            fmt=SYS_LOG_FORMAT
        )
        log_handler = logging.handlers.SysLogHandler(
            address=CFG.logsocket or '/dev/log',
            facility=logging.handlers.SysLogHandler.LOG_DAEMON,
        )
    log_handler.setFormatter(logging.Formatter(fmt=log_format))
    the_logger = logging.getLogger()
    the_logger.addHandler(log_handler)
    if log_level is None:
        log_level = logging.INFO
    the_logger.setLevel(log_level)
    return  # end of init_logger()


def init_runtimedir(runtime_dir):
    """
    create run-time directory and set ownership and permissions
    """
    if os.getuid() != 0:
        logging.debug('Started as non-root user, leave run-time directory %r as is', runtime_dir)
        return
    try:
        if not os.path.exists(runtime_dir):
            logging.debug('Creating run-time directory %r', runtime_dir)
            os.mkdir(runtime_dir)
        logging.debug('Set permissions and ownership of run-time directory %r', runtime_dir)
        os.chown(runtime_dir, CFG.uid, CFG.gid)
        os.chmod(runtime_dir, 0755)
    except (IOError, OSError) as err:
        logging.warn(
            'Failed setting permissions and ownership of run-time directory %r: %s',
            runtime_dir,
            err,
        )
    return # endof init_runtimedir()


def init_service(service_desc, service_uid=None, service_gid=None):
    """
    initialize the service instance
    """
    script_name = os.path.basename(sys.argv[0])
    # extract command-line arguments
    args = cli_args(script_name, service_desc)
    # read configuration file
    cfg.read_config(args.cfg_filename)
    init_logger(script_name, args.log_level, args.no_fork)
    logging.info(
        'Starting %s %s [%d] reading config %s',
        script_name, __version__,
        os.getpid(),
        args.cfg_filename,
    )
    # log config currently effective options
    for key, val in sorted(CFG.__dict__.items()):
        logging.debug('%s = %r', key, val)
    # clean the environment
    os.environ.clear()
    os.environ['HOME'] = '/'
    os.environ['TMPDIR'] = os.environ['TMP'] = '/tmp'
    os.environ['LDAPNOINIT'] = '1'
    # set log level
    if args.log_level is None:
        logging.getLogger().setLevel(CFG.loglevel)
    # set a default umask for the pidfile and socket
    os.umask(UMASK_DEFAULT)
    # see if someone already locked the pidfile
    pidfile = PIDLockFile(args.pidfile)
    runtime_dir = os.path.dirname(CFG.socketpath)
    init_runtimedir(runtime_dir)
    # see if --check option was given
    if args.check_only:
        if pidfile.is_locked():
            logging.debug('pidfile (%s) is locked', args.pidfile)
            sys.exit(0)
        else:
            logging.debug('pidfile (%s) is not locked', args.pidfile)
            sys.exit(1)
    # normal check for pidfile locked
    if pidfile.is_locked():
        logging.error(
            'daemon may already be active, cannot acquire lock (%s)',
            args.pidfile,
        )
        sys.exit(1)
    # daemonize
    if args.no_fork:
        ctx = pidfile
    else:
        if service_uid is None:
            demon_uid = CFG.uid
        else:
            demon_uid = service_uid
        if service_gid is None:
            demon_gid = CFG.gid
        else:
            demon_gid = service_gid
        ctx = daemon.DaemonContext(
            pidfile=pidfile,
            umask=UMASK_DEFAULT,
            uid=demon_uid,
            gid=demon_gid,
            signal_map={
                signal.SIGTERM: u'terminate',
                signal.SIGINT: u'terminate',
                signal.SIGPIPE: None,
            }
        )
    return script_name, ctx # end of init_service()


class TIOStreamRequestHandler(SocketServer.BaseRequestHandler):
    """
    handling a TIO stream request for NSS/PAM
    """

    def _get_peer_cred(self):
        try:
            so_num, struct_fmt = SO_PEERCRED_DICT[sys.platform]
        except KeyError:
            return None, None, None
        peer_creds_struct = self.request.getsockopt(
            socket.SOL_SOCKET,
            so_num,
            struct.calcsize(struct_fmt)
        )
        pid, uid, gid = struct.unpack(struct_fmt, peer_creds_struct)
        return pid, uid, gid  # _get_peer_cred()

    def handle(self):
        """
        handle a single request
        """
        start_time = time.time()
        self.server._req_counter_all += 1
        req_file = self.request.makefile()
        # create a stream object
        tios = TIOStream(req_file)
        # read request
        nslcd_version = tios.read_int32()
        if nslcd_version != req.NSLCD_VERSION:
            logging.error(
                'Wrong nslcd version: Expected %r but got %r',
                req.NSLCD_VERSION,
                nslcd_version
            )
            return
        action = tios.read_int32()
        logging.debug('Incoming request on %s', self.request.getsockname())
        try:
            handler_class = self.server._reqh[action]
        except KeyError:
            logging.error('No handler for action 0x%08x', action)
            self.server._invalid_requests += 1
            return
        self.server._req_counter[action] += 1
        handler = handler_class(
            tios,
            self.server,
            self._get_peer_cred(),
        )
        handler()
        req_time = 1000 * (time.time() - start_time)
        self.server._avg_response_time = (self.server._avg_response_time*30 + req_time) / 31
        self.server._max_response_time = max(self.server._max_response_time, req_time)
        return # end of TIOStreamRequestHandler.handle()


class NSSPAMServer(SocketServer.UnixStreamServer):
    """
    the NSS/PAM socket server
    """

    def __init__(
            self,
            server_address,
            bind_and_activate=True
        ):
        SocketServer.UnixStreamServer.__init__(
            self,
            server_address,
            TIOStreamRequestHandler,
            bind_and_activate,
        )
        self._start_time = time.time()
        self._last_access_time = 0
        self._req_counter_all = 0
        self._invalid_requests = 0
        self._bytes_sent = 0
        self._bytes_received = 0
        self._avg_response_time = 0
        self._max_response_time = 0
        # initialize a map of request handler classes
        self._reqh = {}
        self._reqh.update(req.get_handlers('config'))
        self._reqh.update(req.get_handlers('group'))
        self._reqh.update(req.get_handlers('hosts'))
        self._reqh.update(req.get_handlers('passwd'))
        self._reqh.update(req.get_handlers('pam'))
        # initialize a map of per-action request counters
        self._req_counter = {}.fromkeys(self._reqh.keys(), 0)

    def get_monitor_data(self):
        """
        returns all monitoring data as
        """
        return dict(
            req_count=self._req_counter_all,
            req_err=self._invalid_requests,
            avg_response_time=self._avg_response_time,
            max_response_time=self._max_response_time,
        )

    def server_bind(self):
        """Override server_bind to set socket options."""
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.settimeout(CFG.sockettimeout)
        try:
            os.unlink(self.server_address)
        except OSError:
            if os.path.exists(self.server_address):
                raise
        SocketServer.UnixStreamServer.server_bind(self)
        os.chmod(self.server_address, int(CFG.socketperms, 8))
        logging.debug('%s now accepting connections on %r', self.__class__.__name__, self.server_address)
        return # server_bind()

"""
aehostd.refresh - various worker threads for data refreshing
"""

from __future__ import absolute_import

import os
import glob
import time
import logging
import pprint
import threading
import random
import subprocess
from StringIO import StringIO

import ldap0
from ldap0.ldif import LDIFWriter
from ldap0.functions import strf_secs
from ldap0.controls.deref import DereferenceControl

from .base import IdempotentFile, dict_del
from .cfg import CFG
from . import passwd
from . import group
from . import hosts
from .ldapconn import LDAP_CONN


SUDOERS_ATTRS = [
    'cn',
    'objectClass',
    'sudoCommand',
    'sudoHost',
    'sudoNotAfter', 'sudoNotBefore',
    'sudoOption', 'sudoOrder',
    'sudoRunAs', 'sudoRunAsGroup', 'sudoRunAsUser', 'sudoUser',
]


class RefreshThread(threading.Thread):
    """
    Update thread for retrieving SSH authorized keys and sudoers entries

    Thread is initialized by NSSPAMServer, started by main script
    """
    rand_factor = 2.0
    avg_window = 30.0

    def __init__(self, refresh_sleep):
        threading.Thread.__init__(
            self,
            group=None,
            target=None,
            name=None,
            args=(),
            kwargs={}
        )
        self.enabled = True
        self.schedule_interval = 0.4
        self._refresh_sleep = refresh_sleep
        self._rand = random.SystemRandom()
        self.refresh_counter = 0
        self.avg_refresh_time = 0.0
        self.reset()

    def _log(self, log_level, msg, *args, **kwargs):
        msg = ' '.join((self.__class__.__name__, msg))
        logging.log(log_level, msg, *args, **kwargs)

    def _refresh_task(self, ldap_conn):
        """
        refresh task
        """
        raise NotImplementedError

    def get_monitor_data(self):
        """
        returns all monitoring data as
        """
        return dict(
            refresh_count=self.refresh_counter,
            avg_refresh_time=self.avg_refresh_time,
        )

    def reset(self):
        """
        trigger next run, skips refresh sleep time
        """
        # simply reset run timestamps
        self._last_run = 0.0
        self._next_run = time.time()
        self._log(logging.INFO, 'Reset')

    def run(self):
        """
        retrieve data forever
        """
        self._log(logging.DEBUG, 'Starting %s.run()', self.__class__.__name__)
        while self.enabled:
            start_time = time.time()
            if start_time > self._next_run:
                self._log(logging.DEBUG, 'Invoking %s._refresh_task()', self.__class__.__name__)
                try:
                    ldap_conn = LDAP_CONN.get_ldap_conn()
                    if ldap_conn is None:
                        self._log(
                            logging.WARN,
                            'No valid LDAP connection => abort',
                        )
                    else:
                        self._refresh_task(ldap_conn)
                        self.refresh_counter += 1
                        refresh_time = time.time() - start_time
                        avg_window = min(self.avg_window, self.refresh_counter)
                        self.avg_refresh_time = ((avg_window - 1) * self.avg_refresh_time + refresh_time) / avg_window
                        self._log(
                            logging.INFO,
                            '%d. refresh run with %s (%0.3f secs, avg: %0.3f secs)',
                            self.refresh_counter,
                            ldap_conn.uri,
                            refresh_time,
                            self.avg_refresh_time,
                        )
                except ldap0.SERVER_DOWN, ldap_error:
                    self._log(
                        logging.WARN,
                        'Invalid connection: %s',
                        ldap_error,
                    )
                    LDAP_CONN.disable_ldap_conn()
                except Exception:
                    self._log(
                        logging.ERROR,
                        'Aborted refresh with unhandled exception',
                        exc_info=True,
                    )
                self._last_run = start_time
                self._next_run = (
                    time.time() +
                    self._refresh_sleep +
                    self.rand_factor * self._rand.random()
                )
            time.sleep(self.schedule_interval)
        self._log(logging.DEBUG, 'Exiting %s.run()', self.__class__.__name__)


def visudo_check_cmd(sudoers_filename):
    """
    return command arguments for running visudo to check the given file
    """
    return [CFG.visudo_exec, '-c', '-s', '-q', '-f', sudoers_filename]


class UsersUpdater(RefreshThread):
    """
    Thread spawned to update user and group map caches
    """
    posix_account_attrs = [
        'aeRemoteHost',
        'uidNumber',
        'sshPublicKey',
    ]

    def __init__(self, refresh_sleep):
        RefreshThread.__init__(self, refresh_sleep)
        # at this time CFG.aehost_vaccount is ready,
        # so let's first initialize global passwd map here
        passwd.PASSWD_MAP.update({CFG.aehost_vaccount[2]: CFG.aehost_vaccount})
        passwd.PASSWD_NAME_MAP.update({CFG.aehost_vaccount[0]: CFG.aehost_vaccount[2]})
        group.GROUP_MEMBER_MAP = {CFG.aehost_vaccount[0]: []}
        if CFG.homedir_tmpl is None:
            self.posix_account_attrs.append('homeDirectory')
        if CFG.loginshell_override is None:
            self.posix_account_attrs.append('loginShell')
        self. srvgrp_deref_ctrl = DereferenceControl(
            True,
            {
                'aeVisibleGroups': ['gidNumber', CFG.memberattr],
                'aeVisibleSudoers': SUDOERS_ATTRS,
            }
        )
        # stuff for sudoers
        if CFG.sudoers_file:
            self.ldif_filename = CFG.sudoers_file+'.ldif'
            self.sudoers_tmp_filename = CFG.sudoers_file+'.tmp'
            self.cvtsudoers_cmd = [
                CFG.cvtsudoers_exec,
                '-d', 'all',
                '-i', 'LDIF',
                '-f', 'sudoers',
                '-o', self.sudoers_tmp_filename,
                self.ldif_filename,
            ]
            self.visudo_check_cmd = visudo_check_cmd(self.sudoers_tmp_filename)

    @staticmethod
    def _passwd_convert(entry):
        """
        convert an LDAP entry dict to a passwd map tuple
        """
        name = entry['uid'][0]
        uid = int(entry['uidNumber'][0])
        # primary GID as UID in case of unique primary user GIDs
        gid = uid
        gecos = entry.get(
            'cn',
            [CFG.gecos_tmpl.format(username=name)]
        )[0]
        if CFG.homedir_tmpl:
            home = CFG.homedir_tmpl.format(username=name)
        else:
            home = entry['homeDirectory'][0]
        if CFG.loginshell_override is None:
            shell = entry.get('loginShell', [CFG.loginshell_default])[0]
        else:
            shell = CFG.loginshell_override
        return (name, 'x', uid, gid, gecos, home, shell)
        # end of UsersUpdater._passwd_convert()

    @staticmethod
    def _group_convert(entry):
        """
        convert an LDAP entry dict to a group map tuple
        """
        logging.debug('group_convert(): %r', entry)
        return (
            entry['cn'][0],
            'x',
            int(entry['gidNumber'][0]),
            entry.get('memberUid', tuple()),
        )
        # end of UsersUpdater._group_convert()

    def _store_ssh_key(self, user_entry):
        user_name = user_entry['uid'][0]
        self._log(
            logging.DEBUG,
            'Found user %r with %d SSH keys',
            user_name,
            len(user_entry['sshPublicKey']),
        )
        raddr_list = [
            av.strip()
            for av in user_entry.get('aeRemoteHost', [])
            if av.strip()
        ]
        if raddr_list:
            self._log(logging.DEBUG, 'Attribute aeRemoteHost contains: %r', raddr_list)
            ssh_key_prefix = 'from="%s" ' % (','.join(raddr_list))
        else:
            ssh_key_prefix = ''
        self._log(logging.DEBUG, 'ssh_key_prefix = %r', ssh_key_prefix)
        new_user_ssh_keys = sorted([
            ''.join((ssh_key_prefix, ssh_key.strip()))
            for ssh_key in user_entry['sshPublicKey']
        ])
        sshkey_file = IdempotentFile(os.path.join(CFG.sshkeys_dir, user_name))
        sshkey_file.write('\n'.join(new_user_ssh_keys), mode=0640)
        return # end of store_ssh_key()

    def _ldifstring(self, ldap_results, entry_comment):
        """
        return ldap_results as LDIF string
        """
        ldif_file = StringIO()
        ldif_writer = LDIFWriter(ldif_file)
        for dn, entry in ldap_results:
            self._log(logging.DEBUG, 'Found %s entry %r: %r', entry_comment, dn, entry)
            ldif_writer.unparse(dn, entry)
        if len(ldap_results) != ldif_writer.records_written:
            self._log(
                logging.WARN,
                '%d entries to be exported, but only wrote %d',
                len(ldap_results),
                ldif_writer.records_written,
            )
        return ldif_file.getvalue()

    def _export_sudoers(self, sudoers_result):
        """
        write sudoers entries to LDIF file and convert it
        """
        if sudoers_result:
            ldif_str = self._ldifstring(sudoers_result, 'sudoers')
            self._log(
                logging.DEBUG,
                'Added %d sudoers entries to LDIF buf (%d bytes)',
                len(sudoers_result),
                len(ldif_str),
            )
        else:
            ldif_str = '# No sudoers entries found\n'
            self._log(
                logging.DEBUG,
                'No sudoers entries to be exported => dummy comment line',
            )
        ldif_file = IdempotentFile(self.ldif_filename)
        if not ldif_file.write(ldif_str, mode=0640, remove=True):
            return
        if not os.path.exists(self.ldif_filename):
            self._log(
                logging.ERROR,
                'LDIF sudoers file %r does not exist!',
                self.ldif_filename,
            )
            return
        # Use cvssudoers to convert LDIF to sudoers file
        self._log(
            logging.DEBUG,
            'Converting LDIF to sudoers file: %r',
            self.cvtsudoers_cmd,
        )
        cvssudoers_rc = subprocess.call(self.cvtsudoers_cmd, shell=False)
        if cvssudoers_rc != 0:
            self._log(
                logging.ERROR,
                'Converting to sudoers file %r failed with return code %d, command was: %r',
                self.sudoers_tmp_filename,
                cvssudoers_rc,
                self.cvtsudoers_cmd,
            )
            return
        # Check syntax of sudoers file with visudo
        self._log(
            logging.DEBUG,
            'Checking sudoers file: %r',
            self.visudo_check_cmd,
        )
        visudo_rc = subprocess.call(self.visudo_check_cmd, shell=False)
        if visudo_rc != 0:
            self._log(
                logging.ERROR,
                'Checking sudoers file %r failed with return code %d, command was: %r',
                self.sudoers_tmp_filename,
                visudo_rc,
                self.visudo_check_cmd,
            )
            return
        os.chmod(self.sudoers_tmp_filename, 0440)
        os.rename(self.sudoers_tmp_filename, CFG.sudoers_file)
        self._log(
            logging.INFO,
            'Successfully updated sudoers file %s with %d entries',
            CFG.sudoers_file,
            len(sudoers_result),
        )
        return # end of _export_sudoers()

    def _get_group_maps(self, ldap_conn):
        """
        initialize group map and search LDAP groups
        """
        role_groups = dict([
            (role_attr, set())
            for role_attr in CFG.vgroup_role_map.keys()
        ])
        # init group map dictionaries
        group_map = {}
        group_name_map = {}
        group_dn2id_map = {}
        for group_id, group_name in CFG.vgroup_role_map.values():
            group_map[group_id] = UsersUpdater._group_convert({
                'cn': [group_name],
                'gidNumber': [group_id],
                'memberUid': set(),
            })
            group_name_map[group_name] = group_id
        # query the service groups including dereferenced group entries
        msg_id = ldap_conn.search_service_groups(
            ldap_conn.get_whoami_dn(),
            attrlist=CFG.vgroup_role_map.keys(),
            serverctrls=[self.srvgrp_deref_ctrl],
        )
        group_results = []
        sudoers_results = []
        for _, res_data, _, _ in ldap_conn.results(msg_id, add_ctrls=1):
            for _, entry, controls in res_data:
                for role_attr in CFG.vgroup_role_map.keys():
                    role_groups[role_attr].update(entry.get(role_attr, []))
                for ctrl in controls:
                    if ctrl.controlType == DereferenceControl.controlType:
                        group_results.extend(ctrl.derefRes.get('aeVisibleGroups', []))
                        sudoers_results.extend(ctrl.derefRes.get('aeVisibleSudoers', []))
        # export sudoers file if configured
        if CFG.sudoers_file:
            self._export_sudoers(sudoers_results)
        # build and convert complete group entry
        all_user_names = set()
        for group_dn, group_entry in group_results:
            self._log(logging.DEBUG, 'Found group entry %r : %r', group_dn, group_entry)
            # extract group name from entry DN
            group_name = group_dn.split(',', 1)[0][3:]
            gid_number = int(group_entry['gidNumber'][0])
            group_entry['cn'] = [group_name]
            if 'memberUid' not in group_entry:
                # fall-back to member attribute
                group_entry['memberUid'] = [
                    user_dn.split(',', 1)[0][4:]
                    for user_dn in group_entry.get('member', [])
                ]
            all_user_names.update(group_entry['memberUid'])
            group_map[gid_number] = UsersUpdater._group_convert(group_entry)
            group_name_map[group_name] = gid_number
            group_dn2id_map[group_dn] = gid_number
            self._log(logging.DEBUG, 'Group entry %r : %r', group_dn, group_map[gid_number])
            for role_attr in role_groups:
                if 'memberUid' in group_entry and group_dn in role_groups[role_attr]:
                    role_gid_number = CFG.vgroup_role_map[role_attr][0]
                    group_map[role_gid_number][3].update(group_entry['memberUid'])
        self._log(logging.DEBUG, 'Role group mappings: %r', role_groups)
        # build the user membership dict
        group_member_map = {CFG.aehost_vaccount[0]: set()}
        for group_map_entry in group_map.values():
            gid_number = group_map_entry[2]
            for user_name in group_map_entry[3]:
                try:
                    group_member_map[user_name].add(gid_number)
                except KeyError:
                    group_member_map[user_name] = set([gid_number])
        if len(group_map) != len(group_results)+len(CFG.vgroup_role_map):
            self._log(
                logging.WARN,
                'Different group length! group_map=%d group_results=%d',
                len(group_map),
                len(group_results),
            )
        return group_map, group_name_map, group_dn2id_map, group_member_map, role_groups, all_user_names
        # end of UsersUpdater._get_group_maps()

    def _get_passwd_maps(self, ldap_conn, group_map, group_dn2id_map, role_groups):
        # init passwd map dictionaries
        passwd_map = {CFG.aehost_vaccount[2]: CFG.aehost_vaccount}
        passwd_name_map = {CFG.aehost_vaccount[0]: CFG.aehost_vaccount[2]}
        user_group_dn_list = group_dn2id_map.keys()
        if not user_group_dn_list:
            self._log(logging.WARN, 'No visible groups at all => skip searching users')
            return passwd_map, passwd_name_map
        full_user_refresh = (
            role_groups['aeVisibleGroups'] != self._last_role_groups['aeVisibleGroups'] or
            role_groups['aeLoginGroups'] != self._last_role_groups['aeLoginGroups']
        )
        if full_user_refresh:
            # groups did not change => we can do delta-search for users changed after last run
            user_from_timestamp = 0
        else:
            # groups changed => do full search for users changed until now
            user_from_timestamp = self._last_run
        user_filter = '(&{memberof}(modifyTimestamp>={timestamp}))'.format(
            memberof=ldap0.filter.compose_filter(
                '|',
                ldap0.filter.map_filter_parts('memberOf', user_group_dn_list),
            ),
            timestamp=strf_secs(user_from_timestamp),
        )
        self._log(logging.DEBUG, 'Search users with filter %r', user_filter)
        # search user entries
        msg_id = ldap_conn.search(
            ldap_conn.find_search_base(),
            ldap0.SCOPE_SUBTREE,
            filterstr=user_filter,
            attrlist=self.posix_account_attrs,
        )
        if msg_id is None:
            self._log(
                logging.WARN,
                'Searching users with filter %r failed (msg_id = %r)',
                user_filter,
                msg_id
            )
            return passwd_map, passwd_name_map
        sshkeys_usernames = set()
        for _, res, _, _ in ldap_conn.results(msg_id, timeout=CFG.timelimit):
            for user_dn, user_entry in res:
                self._log(logging.DEBUG, 'Found user entry %r : %r', user_dn, user_entry)
                # extract user account name from DN
                user_name = user_dn.split(',', 1)[0][4:]
                user_entry['uid'] = [user_name]
                uid_number = int(user_entry['uidNumber'][0])
                passwd_map[uid_number] = UsersUpdater._passwd_convert(user_entry)
                passwd_name_map[user_name] = uid_number
                # store user's SSH key
                if 'sshPublicKey' in user_entry:
                    self._store_ssh_key(user_entry)
                    sshkeys_usernames.add(user_name)
                else:
                    self._delete_ssh_key(user_name)
        # trigger removal of obsolete SSH keys
        if full_user_refresh:
            self._delete_obsolete_keys(sshkeys_usernames)
        return passwd_map, passwd_name_map
        # end of UsersUpdater._get_passwd_maps()

    def _delete_ssh_key(self, user_name):
        sshkey_filename = os.path.join(CFG.sshkeys_dir, user_name)
        if not os.path.exists(sshkey_filename):
            self._log(logging.DEBUG, 'No SSH key file %r found', sshkey_filename)
            return
        self._log(logging.INFO, 'Removing SSH key file %r', sshkey_filename)
        try:
            os.remove(sshkey_filename)
        except OSError, os_error:
            self._log(
                logging.ERROR,
                'Error removing SSH key file %r: %r',
                sshkey_filename,
                os_error,
            )

    def _delete_obsolete_keys(self, active_userid_set):
        """
        remove SSH keys for usernames not in `active_userid_set'
        """
        existing_ssh_key_files = glob.glob(os.path.join(CFG.sshkeys_dir, '*'))
        path_prefix_len = len(CFG.sshkeys_dir) + 1
        self._log(
            logging.DEBUG,
            '%d existing SSH key files found: %r',
            len(existing_ssh_key_files),
            existing_ssh_key_files
        )
        old_userid_set = set([
            p[path_prefix_len:]
            for p in existing_ssh_key_files
        ])
        self._log(
            logging.DEBUG,
            '%d existing user IDs: %s',
            len(old_userid_set),
            ', '.join(map(str, old_userid_set))
        )
        to_be_removed = old_userid_set - active_userid_set
        if to_be_removed:
            self._log(
                logging.INFO,
                '%d existing files to be removed: %s',
                len(to_be_removed),
                ', '.join(map(str, to_be_removed))
            )
            for user_name in to_be_removed:
                self._delete_ssh_key(user_name)
        return # end of SSHKeysUpdater._refresh_task()

    def _refresh_task(self, ldap_conn):
        """
        Search users and groups
        """

        # init map dictionaries and search map entries
        #-------------------------------------------------------------------
        group_map, group_name_map, group_dn2id_map, group_member_map, \
            role_groups, all_user_names = self._get_group_maps(ldap_conn)
        passwd_map, passwd_name_map = self._get_passwd_maps(
            ldap_conn, group_map, group_dn2id_map, role_groups
        )

        # determine UID set of all user names seen in group entries
        #-------------------------------------------------------------------
        self._log(logging.DEBUG, 'all_user_names = %r', all_user_names)
        new_passwd_keys = set()
        for user_name in list(all_user_names):
            if user_name in passwd_name_map:
                new_passwd_keys.add(passwd_name_map[user_name])
            elif user_name in passwd.PASSWD_NAME_MAP:
                new_passwd_keys.add(passwd.PASSWD_NAME_MAP[user_name])
            else:
                self._log(logging.WARN, 'Could not map user name %r to UID', user_name)
        new_passwd_keys.add(CFG.aehost_vaccount[2])
        self._log(logging.DEBUG, 'new_passwd_keys = %r', new_passwd_keys)

        # update global passwd map dictionaries
        #-------------------------------------------------------------------
        add_passwd_keys = new_passwd_keys - passwd.PASSWD_MAP_VKEYS
        remove_passwd_keys = passwd.PASSWD_MAP_VKEYS - new_passwd_keys
        passwd.PASSWD_MAP.update(passwd_map)
        passwd.PASSWD_NAME_MAP.update(passwd_name_map)
        group.GROUP_MEMBER_MAP.update(group_member_map)
        if add_passwd_keys:
            self._log(
                logging.INFO,
                '%d passwd entries added: %s',
                len(add_passwd_keys),
                ','.join([
                    passwd.PASSWD_MAP[uid_number][0]
                    for uid_number in add_passwd_keys
                ])
            )
        if remove_passwd_keys:
            remove_passwd_names = ','.join([
                passwd.PASSWD_MAP[uid_number][0]
                for uid_number in remove_passwd_keys
            ])
            for uid_number in remove_passwd_keys:
                self._log(logging.DEBUG, 'Removing %r from passwd map', passwd.PASSWD_MAP[uid_number][0])
                dict_del(passwd.PASSWD_NAME_MAP, passwd.PASSWD_MAP[uid_number][0])
                dict_del(group.GROUP_MEMBER_MAP, passwd.PASSWD_MAP[uid_number][0])
                del passwd.PASSWD_MAP[uid_number]
            self._log(
                logging.INFO,
                '%d passwd entries removed: %s',
                len(remove_passwd_keys),
                remove_passwd_names,
            )
        self._log(logging.DEBUG, '%d passwd entries', len(passwd.PASSWD_MAP))
        if not (
                len(passwd.PASSWD_MAP) ==
                len(passwd.PASSWD_NAME_MAP) ==
                len(group.GROUP_MEMBER_MAP)
            ):
            self._log(
                logging.WARN,
                (
                    'Different map length!'
                    'PASSWD_MAP=%d PASSWD_NAME_MAP=%d GROUP_MEMBER_MAP=%d'
                ),
                len(passwd.PASSWD_MAP),
                len(passwd.PASSWD_NAME_MAP),
                len(group.GROUP_MEMBER_MAP),
            )
            self._log(
                logging.DEBUG,
                'PASSWD_MAP = %s',
                pprint.pformat(passwd.PASSWD_MAP, indent=2),
            )
            self._log(
                logging.DEBUG,
                'PASSWD_NAME_MAP = %s',
                pprint.pformat(passwd.PASSWD_NAME_MAP, indent=2),
            )
            self._log(
                logging.DEBUG,
                'GROUP_MEMBER_MAP = %s',
                pprint.pformat(group.GROUP_MEMBER_MAP, indent=2),
            )

        # augment posixGroup entries with virtual groups
        # derived from posixAccount entries
        #-------------------------------------------------------------------
        for uid_number in new_passwd_keys:
            pw_entry = passwd.PASSWD_MAP[uid_number]
            # Safety first! Do not assume UID and GID are equal!
            gid_number = pw_entry[2]
            if gid_number in group_map:
                continue
            group_name = ''.join((
                CFG.vgroup_name_prefix,
                pw_entry[0],
            ))
            group_map[gid_number] = UsersUpdater._group_convert({
                'cn': [group_name],
                'gidNumber': [gid_number],
            })
            group_name_map[group_name] = gid_number
            self._log(logging.DEBUG, 'Primary user group entry for %d : %r', gid_number, group_map[gid_number])

        # update global group map dictionaries
        #-------------------------------------------------------------------
        new_group_keys = group_map.viewkeys()
        add_group_keys = new_group_keys - group.GROUP_MAP_VKEYS
        remove_group_keys = group.GROUP_MAP_VKEYS - new_group_keys
        group.GROUP_MAP.update(group_map)
        group.GROUP_NAME_MAP.update(group_name_map)
        if add_group_keys:
            self._log(
                logging.INFO,
                '%d group entries added: %s',
                len(add_group_keys),
                ','.join([
                    group.GROUP_MAP[group_dn][0]
                    for group_dn in add_group_keys
                ])
            )
        if remove_group_keys:
            remove_group_names = ','.join([
                group.GROUP_MAP[gid_number][0]
                for gid_number in remove_group_keys
            ])
            for gid_number in remove_group_keys:
                dict_del(group.GROUP_NAME_MAP, group.GROUP_MAP[gid_number][0])
                del group.GROUP_MAP[gid_number]
                self._log(logging.DEBUG, 'Removed %d from group map', gid_number)
            self._log(
                logging.INFO,
                '%d group entries removed: %s',
                len(remove_group_keys),
                remove_group_names,
            )
        self._log(logging.DEBUG, '%d group entries', len(group.GROUP_MAP))

        # save state
        self._last_role_groups = role_groups

        return # end of _refresh_task()

    def reset(self):
        """
        trigger next run, skips refresh sleep time
        """
        # reset state for delta sync
        self._last_role_groups = dict([
            (role_attr, set())
            for role_attr in CFG.vgroup_role_map.keys()
        ])
        RefreshThread.reset(self)


class NetworkAddrUpdater(RefreshThread):
    """
    Thread spawned to update hosts map cache
    """
    hosts_attrs = [
        'aeFqdn',
        'ipHostNumber',
        'macAddress',
    ]

    def _refresh_task(self, ldap_conn):
        """
        Refresh the hosts map
        """
        hosts_map = {}
        hosts_name_map = {}
        hosts_addr_map = {}
        netaddr_base = ','.join(ldap0.dn.explode_dn(ldap_conn.get_whoami_dn())[CFG.netaddr_level:])
        self._log(logging.DEBUG, 'Searching network address entries beneath %r', netaddr_base)
        ldap_results = ldap_conn.search_s(
            netaddr_base,
            ldap0.SCOPE_SUBTREE,
            filterstr='(objectClass=aeNwDevice)',
            attrlist=self.hosts_attrs,
        )
        for nw_dn, nw_entry in ldap_results:
            hosts_map[nw_dn] = nw_entry
            for name in nw_entry['aeFqdn']:
                hosts_name_map[name] = nw_dn
            for addr in nw_entry['ipHostNumber']:
                hosts_addr_map[addr] = nw_dn
        # update hosts map dictionaries
        new_hosts_keys = hosts_map.viewkeys()
        add_hosts_keys = new_hosts_keys - hosts.HOSTS_MAP_VKEYS
        remove_hosts_keys = hosts.HOSTS_MAP_VKEYS - new_hosts_keys
        hosts.HOSTS_MAP.update(hosts_map)
        hosts.HOSTS_NAME_MAP.update(hosts_name_map)
        hosts.HOSTS_ADDR_MAP.update(hosts_addr_map)
        self._log(
            logging.DEBUG,
            '%d hosts entries, added %d, removed %d',
            len(hosts.HOSTS_MAP),
            len(add_hosts_keys),
            len(remove_hosts_keys),
        )
        if remove_hosts_keys:
            for nw_dn in remove_hosts_keys:
                try:
                    ldap_res = ldap_conn.read_s(nw_dn, attrlist=['1.1'])
                    if ldap_res is None:
                        raise ldap0.NO_SUCH_OBJECT()
                except (ldap0.NO_SUCH_OBJECT, ldap0.INSUFFICIENT_ACCESS):
                    for name in hosts.HOSTS_MAP[nw_dn]['aeFqdn']:
                        dict_del(hosts.HOSTS_NAME_MAP, name)
                    for addr in hosts.HOSTS_MAP[nw_dn]['ipHostNumber']:
                        dict_del(hosts.HOSTS_ADDR_MAP, addr)
                    del hosts.HOSTS_MAP[nw_dn]
                    self._log(logging.DEBUG, 'Removed %r from group map', nw_dn)
                else:
                    self._log(
                        logging.WARN,
                        '%r marked to be deleted, but found %r => abort refresh',
                        nw_dn,
                        ldap_res,
                    )
                    return
            self._log(
                logging.DEBUG,
                '%d hosts entries removed: %s',
                len(remove_hosts_keys),
                remove_hosts_keys,
            )
        return # end of _refresh_task()


USERSUPDATER_TASK = None

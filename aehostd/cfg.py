# -*- coding: utf-8 -*-
"""
aehostd.cfg - configuration vars
"""

from __future__ import absolute_import

import os
import socket
import pwd
import grp
import logging
import re
import ConfigParser
import collections

import ldap0.functions

from .base import IdempotentFile


def val_list(cfg_val):
    """
    Splits string with space or comma-separated list of attribute names
    and returns a set with white-space stripped values
    """
    val_set = set()
    res = []
    for val in (cfg_val or '').strip().replace(' ', ',').split(','):
        val = val.strip()
        if val and val not in val_set:
            res.append(val)
            val_set.add(val)
    return res


def rotated_hash_list(cfg_val):
    if not cfg_val:
        return []
    lst = collections.deque(sorted(ldap0.functions.attr_set(cfg_val)))
    lst.rotate(hash(socket.getfqdn()) % len(lst))
    return lst


class ConfigParameters(object):
    """
    method-less class containing all config params
    """
    cfg_type_map = {
        'monitor': float,
        'refresh_sleep': float,
        'search_timelimit': float,
        'cache_ttl': float,
        'conn_ttl': float,
        'loglevel': int,
        'netaddr_level': int,
        'netaddr_refresh': float,
        'nss_max_gid': int,
        'nss_max_uid': int,
        'nss_min_gid': int,
        'nss_min_uid': int,
        'uri_list': val_list,
        'uri_pool': rotated_hash_list,
        'sockettimeout': float,
        'timelimit': int,
        'validnames': re.compile,
        'vgroup_rgid_offset': int,
        'bindpwfile': IdempotentFile,
    }

    # General process parameters
    #-------------------------------------------
    # the user id aehostd should be run as
    uid = None
    # the group id aehostd should be run as
    gid = None
    # level of log details
    loglevel = logging.INFO
    # path name of syslog socket:
    # Setting this to a string enforces using syslog,
    # empty string results in default /dev/log being used
    logsocket = None
    # interval at which monitoring data is written to log
    monitor = -1.0

    # Parameters for the Unix domain socket from which
    # to receive requests from front-end modules
    #-------------------------------------------
    # full path name of service socket
    socketpath = '/var/run/aehostd/socket'
    # timeout of service socket
    sockettimeout = 10.0
    # permissions used for service socket
    socketperms = '0666'

    # LDAP connection parameters
    #-------------------------------------------
    # the LDAP server to use
    uri_list = []
    uri_pool = []
    # the DN to use when binding
    binddn = None
    bindpwfile = IdempotentFile('/var/lib/aehostd/aehostd.pw')
    # timing configuration
    timelimit = 3.0
    # LDAPObject cache TTL
    cache_ttl = 6.0
    # SSL/TLS options
    tls_cacertfile = None
    tls_cert = None
    tls_key = None
    conn_ttl = 1800.0

    # Parameters related to NSS maps
    #-------------------------------------------
    # passwd entries to ignore
    nss_ignore_users = set([x.pw_name for x in pwd.getpwall()])
    nss_ignore_uids = set([x.pw_uid for x in pwd.getpwall()])
    # refresh time for NSS passwd and group maps
    refresh_sleep = 60.0
    # group entries to ignore
    nss_ignore_groups = set([x.gr_name for x in grp.getgrall()])
    nss_ignore_gids = set([x.pw_gid for x in pwd.getpwall()])
    # POSIX-ID and name validation
    nss_min_uid = 0
    nss_min_gid = 0
    nss_max_uid = 65500
    nss_max_gid = 65500
    validnames = re.compile(r'^[a-zA-Z0-9._@$][a-zA-Z0-9._@$ \\~-]{0,98}[a-zA-Z0-9._@$~-]$')
    # network addresses
    netaddr_refresh = -1.0
    netaddr_level = 2
    # virtual groups
    memberattr = 'memberUid'
    # virtual groups
    vgroup_name_prefix = 'ae-vgrp-'
    vgroup_rgid_offset = 9000
    # sudoers file refreshing
    sudoers_file = None
    sudoers_includedir = '/etc/sudoers.d'
    # SSH authorized keys files refreshing
    sshkeys_dir = None
    # user account used to authenticate as own aeHost object
    aehost_vaccount = ':'.join((
        'aehost-init',
        'x',
        '9042',
        '9042',
        'AE-DIR virtual host init account',
        '/tmp',
        '/bin/true'
    ))
    gecos_tmpl = 'AE-DIR user {username}'
    homedir_tmpl = None
    # login shell to be used if attribute loginShell is not available
    loginshell_default = '/usr/sbin/nologin'
    # login shell always used not matter what's in attribute loginShell
    loginshell_override = None

    # path of executables of sudo installation
    visudo_exec = '/usr/sbin/visudo'
    cvtsudoers_exec = '/usr/bin/cvtsudoers'

    # Parameters related to PAM handling
    #-------------------------------------------
    # PAM vars
    pam_authz_search = None
    pam_passmod_deny_msg = None

    def get_ldap_uris(self):
        return list(self.uri_pool)[:] + list(reversed(self.uri_list))


def read_config(cfg_filename):
    """
    read and parse config file into dict
    """
    if not os.path.exists(cfg_filename):
        raise SystemExit('Configuration file %r is missing!' % (cfg_filename))
    cfg_parser = ConfigParser.RawConfigParser()
    cfg_parser.read([cfg_filename])
    global CFG
    for key in sorted(cfg_parser.options('aehostd')):
        if not hasattr(CFG, key):
            raise ValueError('Unknown config key-word %r' % (key))
        type_func = CFG.cfg_type_map.get(key, str)
        raw_val = cfg_parser.get('aehostd', key)
        try:
            val = type_func(raw_val)
        except ValueError:
            raise ValueError('Invalid value for %r. Expected %s string, but got %r' % (
                key, type_func.__name__, raw_val
            ))
        setattr(CFG, key, val)
    # compose parameters for virtual groups
    CFG.vgroup_role_map = {
        'aeVisibleGroups': (CFG.vgroup_rgid_offset+0, CFG.vgroup_name_prefix+'role-all'),
        'aeLoginGroups': (CFG.vgroup_rgid_offset+1, CFG.vgroup_name_prefix+'role-login'),
        'aeLogStoreGroups': (CFG.vgroup_rgid_offset+2, CFG.vgroup_name_prefix+'role-log'),
        'aeSetupGroups': (CFG.vgroup_rgid_offset+3, CFG.vgroup_name_prefix+'role-setup'),
    }
    CFG.vgroup_gid2attr = dict([
        (val[0], attr)
        for attr, val in CFG.vgroup_role_map.items()
    ])
    CFG.vgroup_name2attr = dict([
        (val[1], attr)
        for attr, val in CFG.vgroup_role_map.items()
    ])
    # some more user config parameters
    passwd_fields = CFG.aehost_vaccount.split(':')
    CFG.aehost_vaccount = (
        passwd_fields[0],
        passwd_fields[1],
        int(passwd_fields[2]),
        int(passwd_fields[3]),
        passwd_fields[4],
        passwd_fields[5],
        passwd_fields[6],
    )
    # derive numeric UID/GID from config vars
    if CFG.uid is None:
        pw_user = pwd.getpwuid(os.getuid())
    else:
        pw_user = pwd.getpwnam(CFG.uid)
    CFG.uid = pw_user.pw_uid
    if CFG.gid is None:
        CFG.gid = pw_user.pw_gid
    else:
        CFG.gid = grp.getgrnam(CFG.gid).gr_gid
    return # end of read_config()


CFG = ConfigParameters()

# -*- coding: utf-8 -*-
"""
aehostd.srv - main service module
"""

from __future__ import absolute_import

import logging
import os

from .__about__ import __version__
from .cfg import CFG
from .service import NSSPAMServer, init_service
from . import monitor
from . import refresh
from .refresh import UsersUpdater, NetworkAddrUpdater


DESCRIPTION = 'NSS/PAM service for AE-DIR'


def main():
    """
    entry point for demon running as non-privileged user
    """
    script_name, ctx = init_service(DESCRIPTION)
    # start service
    with ctx:
        try:
            try:
                logging.debug(
                    'Initializing %s instance listening on %r',
                    NSSPAMServer.__name__,
                    CFG.socketpath,
                )
                server = NSSPAMServer(CFG.socketpath)
                logging.debug('Start refresh thread')
                # build a list of additional back-group threads
                refresh.USERSUPDATER_TASK = UsersUpdater(CFG.refresh_sleep)
                spawned_threads = [refresh.USERSUPDATER_TASK]
                if CFG.netaddr_refresh > 0 and CFG.netaddr_level > 0:
                    spawned_threads.append(NetworkAddrUpdater(CFG.netaddr_refresh))
                if CFG.monitor > 0:
                    spawned_threads.append(
                        monitor.Monitor(
                            CFG.monitor,
                            server,
                            refresh.USERSUPDATER_TASK,
                        )
                    )
                # now start the threads
                for thr in spawned_threads:
                    logging.debug('Starting %s', thr.__class__.__name__)
                    thr.enabled = True
                    thr.start()
                logging.info(
                    '%s instance is listening on %r, start serving requests',
                    server.__class__.__name__,
                    server.server_address,
                )
                server.serve_forever()
            except (KeyboardInterrupt, SystemExit) as exit_exc:
                logging.debug('Exit exception received: %r', exit_exc)
                for thr in spawned_threads:
                    logging.debug('Disabled %s', thr.__class__.__name__)
                    thr.enabled = False
        finally:
            logging.debug('Removing socket path %r', CFG.socketpath)
            try:
                os.remove(CFG.socketpath)
            except OSError as os_error:
                logging.debug('Error removing socket path %r: %s', CFG.socketpath, os_error)
        logging.info('Stopped %s %s', script_name, __version__)
    return # end of main()


if __name__ == '__main__':
    main()

"""
aehostd.monitor - write monitor data
"""

from __future__ import absolute_import

import time
import logging
import json
import threading

from .cfg import CFG
from . import passwd
from . import group
from . import hosts


class Monitor(threading.Thread):
    """
    monitoring thread
    """

    def __init__(self, monitor_interval, server, user_refresh):
        threading.Thread.__init__(
            self,
            group=None,
            target=None,
            name=None,
            args=(),
            kwargs={}
        )
        self.enabled = True
        self._next_run = 0.0
        self._server = server
        self._user_refresh = user_refresh
        self._schedule_interval = 0.2
        self._monitor_interval = monitor_interval

    def _log(self, log_level, msg, *args, **kwargs):
        """
        log one line prefixed with class name
        """
        msg = ' '.join((self.__class__.__name__, msg))
        logging.log(log_level, msg, *args, **kwargs)

    def run(self):
        """
        do the work
        """
        self._log(logging.DEBUG, 'Starting %s.run()', self.__class__.__name__)
        while self.enabled:
            start_time = time.time()
            if start_time >= self._next_run:
                self._log(
                    logging.INFO,
                    '%s %s',
                    self._server.__class__.__name__,
                    json.dumps(self._server.get_monitor_data()),
                )
                self._log(
                    logging.INFO,
                    '%s %s',
                    self._user_refresh.__class__.__name__,
                    json.dumps(self._user_refresh.get_monitor_data()),
                )
                self._next_run = start_time + self._monitor_interval
            time.sleep(self._schedule_interval)
        self._log(logging.DEBUG, 'Exiting %s.run()', self.__class__.__name__)

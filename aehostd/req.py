# -*- coding: utf-8 -*-
"""
aehostd.req - base stuff for processing requests
"""

from __future__ import absolute_import

import logging
import sys

from .cfg import CFG

# The following constants have to match content of nslcd.h
NSLCD_VERSION = 0x00000002
NSLCD_RESULT_BEGIN = 1
NSLCD_RESULT_END = 2


class Request(object):
    """
    Request handler class. Subclasses are expected to handle actual requests
    and should implement the following members:

      action - the NSLCD_ACTION_* action that should trigger this handler

      read_parameters() - a function that reads the request parameters of the
                          request stream
      write() - function that writes a single LDAP entry to the result stream

    """

    def __init__(self, tios, server, peer):
        self.tios = tios
        self.server = server
        self.peer = peer
        self._log_prefix = self._get_log_prefix()
        self.search = getattr(sys.modules[self.__module__], 'Search', None)

    def _get_log_prefix(self):
        pid, uid, gid = self.peer
        return 'pid=%d uid=%d gid=%d %s' % (pid, uid, gid, self.__class__.__name__)

    def _log(self, log_level, msg, *args, **kwargs):
        msg = ' '.join((self._log_prefix, msg))
        logging.log(log_level, msg, *args, **kwargs)

    def read_parameters(self):
        """
        Read and return the input parameters from the input stream
        """
        pass

    def get_results(self, parameters):
        """
        get results for parameters
        """
        return []

    def handle_request(self, parameters):
        """
        This method handles the request based on the parameters read
        with read_parameters().
        """
        res_count = 0
        for res in self.get_results(parameters):
            res_count += 1
            self._log(logging.DEBUG, 'res#%d: %r', res_count, res)
            self.tios.write_int32(NSLCD_RESULT_BEGIN)
            self.write(res)
        if not res_count:
            self._log(logging.DEBUG, 'no result')
        # write the final result code
        self.tios.write_int32(NSLCD_RESULT_END)

    def _log_params(self, parameters, log_level):
        if parameters is not None:
            parameters = dict(parameters)
            for param in ('password', 'oldpassword', 'newpassword'):
                if parameters.get(param):
                    parameters[param] = '***'
        self._log(log_level, '(%r)', parameters)

    def __call__(self):
        parameters = None
        try:
            parameters = self.read_parameters() or {}
            self._log_params(parameters, logging.DEBUG)
            self.tios.write_int32(NSLCD_VERSION)
            self.tios.write_int32(self.action)
            self.handle_request(parameters)
        except (KeyboardInterrupt, SystemExit):
            # simply re-raise the exit exception
            raise
        except Exception:
            self._log_params(parameters, logging.ERROR)
            logging.error('Unhandled exception during processing request:', exc_info=True)
        return # end of Request.__call__()


def get_handlers(module):
    """Return a dictionary mapping actions to Request classes."""
    import inspect
    res = {}
    if isinstance(module, basestring):
        module = __import__(module, globals())
    for _, cls in inspect.getmembers(module, inspect.isclass):
        if issubclass(cls, Request) and hasattr(cls, 'action'):
            res[cls.action] = cls
    return res

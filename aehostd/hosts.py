# -*- coding: utf-8 -*-
"""
aehostd.host - lookup functions for host names and addresses (hosts map)
"""

from __future__ import absolute_import

import logging

from . import req


HOSTS_MAP = {}
HOSTS_MAP_VKEYS = HOSTS_MAP.viewkeys()
HOSTS_NAME_MAP = {}
HOSTS_ADDR_MAP = {}

NSLCD_ACTION_HOST_BYNAME = 0x00050001
NSLCD_ACTION_HOST_BYADDR = 0x00050002
NSLCD_ACTION_HOST_ALL = 0x00050008


def hosts_convert(entry):
    """
    convert an LDAP entry dict to a hosts map tuple
    """
    hostnames = entry['aeFqdn']
    return (hostnames[0], hostnames[1:], entry['ipHostNumber'])


class HostReq(req.Request):

    def write(self, result):
        hostname, aliases, addresses = result
        self.tios.write_string(hostname)
        self.tios.write_stringlist(aliases)
        self.tios.write_int32(len(addresses))
        for address in addresses:
            self.tios.write_address(address)


class HostByNameReq(HostReq):

    action = NSLCD_ACTION_HOST_BYNAME

    def read_parameters(self):
        return dict(aeFqdn=self.tios.read_string())

    def get_results(self, parameters):
        try:
            res = hosts_convert(HOSTS_MAP[HOSTS_NAME_MAP[parameters['aeFqdn']]])
        except KeyError:
            self._log(logging.DEBUG, 'not found %r', parameters)
            return
        yield res


class HostByAddressReq(HostReq):

    action = NSLCD_ACTION_HOST_BYADDR

    def read_parameters(self):
        return dict(ipHostNumber=self.tios.read_address())

    def get_results(self, parameters):
        try:
            res = hosts_convert(HOSTS_MAP[HOSTS_ADDR_MAP[parameters['ipHostNumber']]])
        except KeyError:
            self._log(logging.DEBUG, 'not found %r', parameters)
            return
        yield res


class HostAllReq(HostReq):

    action = NSLCD_ACTION_HOST_ALL

    def get_results(self, parameters):
        for _, host_entry in HOSTS_MAP.items():
            yield hosts_convert(host_entry)

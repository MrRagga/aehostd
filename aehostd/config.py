# -*- coding: utf-8 -*-
"""
aehostd.config - routines for getting configuration information
"""

from __future__ import absolute_import

from .cfg import CFG
from . import req


NSLCD_ACTION_CONFIG_GET = 0x00010001
NSLCD_CONFIG_PAM_PASSWORD_PROHIBIT_MESSAGE = 1


class ConfigGetRequest(req.Request):

    action = NSLCD_ACTION_CONFIG_GET

    def read_parameters(self):
        return dict(cfgopt=self.tios.read_int32())

    def write(self, value):
        self.tios.write_int32(req.NSLCD_RESULT_BEGIN)
        self.tios.write_string(value)
        self.tios.write_int32(req.NSLCD_RESULT_END)

    def handle_request(self, parameters):
        cfgopt = parameters['cfgopt']
        if cfgopt == NSLCD_CONFIG_PAM_PASSWORD_PROHIBIT_MESSAGE:
            self.write(CFG.pam_passmod_deny_msg or '')
        else:
            # return empty response
            self.tios.write_int32(req.NSLCD_RESULT_END)

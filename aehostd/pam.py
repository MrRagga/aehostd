# -*- coding: utf-8 -*-
"""
aehostd.pam - PAM authentication, authorisation and session handling
"""

from __future__ import absolute_import

import logging
import socket

import ldap0
from ldap0.controls.ppolicy import PasswordPolicyControl
from ldap0.filter import escape_filter_chars
from ldap0.controls.sessiontrack import SessionTrackingControl, SESSION_TRACKING_FORMAT_OID_USERNAME
from ldap0.pw import random_string

import aedir

from .cfg import CFG
from . import req
from .passwd import PASSWD_NAME_MAP
from .ldapconn import LDAP_CONN
from . import refresh

# PAM actions
NSLCD_ACTION_PAM_AUTHC = 0x000d0001
NSLCD_ACTION_PAM_AUTHZ = 0x000d0002
NSLCD_ACTION_PAM_SESS_O = 0x000d0003
NSLCD_ACTION_PAM_SESS_C = 0x000d0004
NSLCD_ACTION_PAM_PWMOD = 0x000d0005

# PAM result constants
NSLCD_PAM_SUCCESS = 0
NSLCD_PAM_PERM_DENIED = 6
NSLCD_PAM_AUTH_ERR = 7
NSLCD_PAM_CRED_INSUFFICIENT = 8
NSLCD_PAM_AUTHINFO_UNAVAIL = 9
NSLCD_PAM_USER_UNKNOWN = 10
NSLCD_PAM_MAXTRIES = 11
NSLCD_PAM_NEW_AUTHTOK_REQD = 12
NSLCD_PAM_ACCT_EXPIRED = 13
NSLCD_PAM_SESSION_ERR = 14
NSLCD_PAM_AUTHTOK_ERR = 20
NSLCD_PAM_AUTHTOK_DISABLE_AGING = 23
NSLCD_PAM_IGNORE = 25
NSLCD_PAM_ABORT = 26
NSLCD_PAM_AUTHTOK_EXPIRED = 27

# for generating session IDs
SESSION_ID_LENGTH = 25
SESSION_ID_ALPHABET = (
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "01234567890"
)


class PAMRequest(req.Request):
    """
    base class for handling PAM requests (not directly used)
    """


class PAMAuthcReq(PAMRequest):
    """
    handles PAM authc requests
    """

    action = NSLCD_ACTION_PAM_AUTHC

    def read_parameters(self):
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
            password=self.tios.read_string(),
        )

    def write(self, username, authc, authz, msg):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.NSLCD_RESULT_BEGIN)
        self.tios.write_int32(authc)
        self.tios.write_string(username)
        self.tios.write_int32(authz)
        self.tios.write_string(msg)
        self.tios.write_int32(req.NSLCD_RESULT_END)

    def handle_request(self, parameters):
        """
        handle request, mainly do LDAP simple bind
        """
        user_name = parameters['username']
        if user_name not in PASSWD_NAME_MAP:
            raise ValueError('Invalid user name %r' % user_name)
        # safe vars
        pam_authc = NSLCD_PAM_AUTH_ERR
        pam_authz = NSLCD_PAM_PERM_DENIED
        pam_msg = (NSLCD_PAM_AUTH_ERR, NSLCD_PAM_PERM_DENIED, 'Internal error')
        ppolicy_ctrl = None
        # bind using the specified credentials
        try:
            uris = CFG.get_ldap_uris()
            while True:
                ldap_uri = uris.pop()
                try:
                    # open a separate connection
                    conn = aedir.AEDirObject(
                        ldap_uri,
                        trace_level=0,
                        trace_file=None,
                        trace_stack_limit=5,
                        retry_max=0,
                        timeout=CFG.timelimit or 5.0,
                        cacert_filename=CFG.tls_cacertfile,
                        cache_ttl=0.0,
                    )
                    # do anon search to provoke connection failure
                    search_base = conn.find_search_base()
                except ldap0.SERVER_DOWN:
                    if not uris:
                        raise
                else:
                    break
            if user_name == CFG.aehost_vaccount[0]:
                self._log(logging.DEBUG, 'Use aehostd.conf binddn %r', CFG.binddn)
                user_dn = CFG.binddn
            else:
                self._log(logging.DEBUG, 'Construct short user bind-DN from %r and %r', user_name, search_base)
                user_dn = 'uid=%s,%s' % (user_name, search_base)
            self._log(logging.DEBUG, 'user_dn = %r', user_dn)
            _, _, _, ctrls = conn.simple_bind_s(
                user_dn,
                parameters['password'],
                serverctrls=[
                    PasswordPolicyControl(),
                    SessionTrackingControl(
                        parameters['rhost'],
                        '%s::%s' % (socket.getfqdn(), parameters['service']),
                        SESSION_TRACKING_FORMAT_OID_USERNAME,
                        parameters.get('ruser', None) or user_name,
                    ),
                ],
            )
        except ldap0.INVALID_CREDENTIALS as ldap_err:
            self._log(logging.WARN, 'LDAP simple bind failed for %r: %s', user_dn, ldap_err)
            pam_authc, pam_msg = (NSLCD_PAM_AUTH_ERR, 'Wrong username or password')
        except ldap0.LDAPError as ldap_err:
            self._log(logging.WARN, 'LDAP error checking password for %r: %s', user_dn, ldap_err)
            pam_authc = NSLCD_PAM_AUTH_ERR
        else:
            self._log(logging.DEBUG, 'LDAP simple bind successful for %r on %r', user_dn, ldap_uri)
            pam_authc = pam_authz = NSLCD_PAM_SUCCESS
            if user_name == CFG.aehost_vaccount[0]:
                pam_msg = 'Host password check ok'
                pam_authz = NSLCD_PAM_PERM_DENIED
                CFG.bindpwfile.write(parameters['password'])
                refresh.USERSUPDATER_TASK.reset()
            else:
                pam_msg = 'User password check ok'
            # search password policy response control
            for ctrl in ctrls:
                if ctrl.controlType == PasswordPolicyControl.controlType:
                    ppolicy_ctrl = ctrl
                    break
        if ppolicy_ctrl:
            # found a password policy control
            self._log(
                logging.DEBUG,
                'PasswordPolicyControl: error=%r, timeBeforeExpiration=%r, graceAuthNsRemaining=%r',
                ppolicy_ctrl.error,
                ppolicy_ctrl.timeBeforeExpiration,
                ppolicy_ctrl.graceAuthNsRemaining,
            )
            if ppolicy_ctrl.error == 0:  # password is expired but still grace logins
                pam_authz, pam_msg = (NSLCD_PAM_AUTHTOK_EXPIRED, 'Password expired')
                if ppolicy_ctrl.graceAuthNsRemaining is not None:
                    pam_msg += ', %d grace logins left' % (ppolicy_ctrl.graceAuthNsRemaining)
            elif ppolicy_ctrl.error is None and \
                ppolicy_ctrl.timeBeforeExpiration is not None:
                pam_msg = 'Password will expire in %d seconds' % (ppolicy_ctrl.timeBeforeExpiration)
        if pam_authc == NSLCD_PAM_SUCCESS and pam_authz == NSLCD_PAM_SUCCESS:
            pam_log_level = logging.DEBUG
        else:
            pam_log_level = logging.WARN
        self._log(
            pam_log_level,
            'PAM auth result for %r: authc=%d authz=%d msg=%r',
            user_name, pam_authc, pam_authz, pam_msg
        )
        self.write(user_name, pam_authc, pam_authz, pam_msg)
        return


class PAMAuthzReq(PAMRequest):
    """
    handles PAM authz requests
    """

    action = NSLCD_ACTION_PAM_AUTHZ

    def read_parameters(self):
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
        )

    def write(self, authz, msg):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.NSLCD_RESULT_BEGIN)
        self.tios.write_int32(authz)
        self.tios.write_string(msg)
        self.tios.write_int32(req.NSLCD_RESULT_END)

    def _check_authz_search(self, parameters):
        if not CFG.pam_authz_search:
            return
        # escape all parameters
        variables = dict((k, escape_filter_chars(v)) for k, v in parameters.items())
        variables.update(
            hostname=escape_filter_chars(socket.gethostname()),
            fqdn=escape_filter_chars(socket.getfqdn()),
            uid=variables['username'],
        )
        filter_tmpl = CFG.pam_authz_search
        if 'rhost' in variables and variables['rhost']:
            filter_tmpl = '(&%s(|(!(aeRemoteHost=*))(aeRemoteHost={rhost})))' % (filter_tmpl)
        ldap_filter = filter_tmpl.format(**variables)
        self._log(logging.DEBUG, 'check authz filter %r', ldap_filter)
        ldap_conn = LDAP_CONN.get_ldap_conn()
        ldap_conn.find_unique_entry(ldap_conn.find_search_base(), filterstr=ldap_filter, attrlist=['1.1'])
        return # end of _check_authz_search()

    def handle_request(self, parameters):
        """
        handle request, mainly do LDAP authz search
        """
        user_name = parameters['username']
        if user_name not in PASSWD_NAME_MAP:
            self._log(logging.WARN, 'Invalid user name %r', user_name)
            self.write(NSLCD_PAM_PERM_DENIED, 'Invalid user name')
            return
        if user_name == CFG.aehost_vaccount[0]:
            self._log(logging.INFO, 'Reject login with host account %r', user_name)
            self.write(NSLCD_PAM_PERM_DENIED, 'Host account ok, but not authorized for login')
            return
        # check authorisation search
        try:
            self._check_authz_search(parameters)
        except ldap0.LDAPError as ldap_err:
            self._log(logging.WARNING, 'authz failed for %s: %s', user_name, ldap_err)
            self.write(NSLCD_PAM_PERM_DENIED, 'LDAP authz check failed')
        except (KeyError, ValueError, IndexError) as err:
            self._log(logging.WARNING, 'Value check failed for %s: %s', user_name, err)
            self.write(NSLCD_PAM_PERM_DENIED, 'LDAP authz check failed')
        else:
            self._log(logging.DEBUG, 'authz ok for %s', user_name)
            # all tests passed, return OK response
            self.write(NSLCD_PAM_SUCCESS, '')


class PAMPassModReq(PAMRequest):
    """
    handles PAM passmod requests
    """

    action = NSLCD_ACTION_PAM_PWMOD

    def read_parameters(self):
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
            asroot=self.tios.read_int32(),
            oldpassword=self.tios.read_string(),
            newpassword=self.tios.read_string(),
        )

    def write(self, res, msg):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.NSLCD_RESULT_BEGIN)
        self.tios.write_int32(res)
        self.tios.write_string(msg)
        self.tios.write_int32(req.NSLCD_RESULT_END)

    def handle_request(self, parameters):
        """
        handle request, just refuse password change
        """
        self.write(NSLCD_PAM_PERM_DENIED, CFG.pam_passmod_deny_msg)
        return


class PAMSessOpenReq(PAMRequest):
    """
    handles PAM session open requests
    """

    action = NSLCD_ACTION_PAM_SESS_O

    def read_parameters(self):
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
        )

    def write(self, sessionid):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.NSLCD_RESULT_BEGIN)
        self.tios.write_string(sessionid)
        self.tios.write_int32(req.NSLCD_RESULT_END)

    def handle_request(self, parameters):
        """
        handle request, mainly return new generated session id
        """
        session_id = random_string(alphabet=SESSION_ID_ALPHABET, length=SESSION_ID_LENGTH)
        self._log(logging.DEBUG, 'New session ID: %s', session_id)
        self.write(session_id)


class PAMSessCloseReq(PAMRequest):
    """
    handles PAM session close requests
    """

    action = NSLCD_ACTION_PAM_SESS_C

    def read_parameters(self):
        return dict(
            username=self.tios.read_string(),
            service=self.tios.read_string(),
            ruser=self.tios.read_string(),
            rhost=self.tios.read_string(),
            tty=self.tios.read_string(),
            session_id=self.tios.read_string(),
        )

    def write(self):
        """
        write result to PAM client
        """
        self.tios.write_int32(req.NSLCD_RESULT_BEGIN)
        self.tios.write_int32(req.NSLCD_RESULT_END)

    def handle_request(self, parameters):
        """
        handle request, do nothing yet
        """
        self.write()
